# Pathfinder 2e PL - Foundry VTT

Nieoficjalne polskie tłumaczenie systemu Pathfinder 2e dla Foundry VTT.

## Instalacja

Skopiuj łącze i użyj w menadżerze modułów Foundry

```
https://gitlab.com/pf2e-pl/pf2e-pl/-/raw/main/module.json
```

## Pomoc w tłumaczeniu

Tłumaczenie jest 100% otwarte, wykonywane i utrzymywane przez społeczność. Jeśli chciałbyś/aś się zaangażować, zapraszamy na stronę: [https://hosted.weblate.org/projects/pathfinder-2e-pl/] gdzie możesz zaproponować swoje zmiany, lub pomóc nam w tłumaczeniu.
